"""
conftest.py file, detected and loaded by pytest. Fixtures in this file can be requested
by tests.
"""
from collections.abc import Iterator
from unittest.mock import patch

from django.conf import settings

import pytest
from rest_framework.test import APIClient

pytest_plugins = ("celery.contrib.pytest",)


@pytest.fixture
def api_client():
    """
    Returns the APIClient for use in tests.
    """
    return APIClient


def pytest_configure(config):  # pylint: disable=unused-argument
    """
    Configure our test environment.
    """
    # Use the fastest, dumbest password hasher we can get our hands on.
    settings.PASSWORD_HASHERS = [
        "django.contrib.auth.hashers.UnsaltedMD5PasswordHasher",
    ]
    settings.DEFAULT_WHITELISTED_DOMAINS = ["heroes.com"]
    settings.EMAIL_BACKEND = "django.core.mail.backends.locmem.EmailBackend"
    settings.CACHE_KEY_DELIMITER = ":"
    settings.CACHES = {
        "default": {"BACKEND": "django.core.cache.backends.dummy.DummyCache"}
    }


@pytest.fixture(scope="session")
def celery_config():
    """
    Test celery config
    """
    return {
        "broker_url": "memory://",
        "task_always_eager": True,
        "task_eager_propagates": True,
    }


@pytest.fixture()
def patch_flush_many_cache() -> Iterator[None]:
    """
    patch flush_many_cache as it is only available for redis cache
    """
    with patch("workflow.signals.flush_many_cache"):
        yield
