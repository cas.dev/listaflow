"""Report module"""

REPORT_FORMAT_QUERY_KEY = "report_format"

REPORT_FORMAT_PDF = "pdf"
REPORT_FORMAT_CSV = "csv"
REPORT_FORMAT_JSON = "json"

ACCEPTABLE_REPORT_FORMATS = set(
    [
        REPORT_FORMAT_PDF,
        REPORT_FORMAT_CSV,
        REPORT_FORMAT_JSON,
    ]
)
