"""Objects for representing interection fields and generating schema."""

# pylint: disable=too-few-public-methods


class BaseObject:
    """Base object class."""

    def __init__(self, description="", default=None):
        self.description = description
        if default is not None:
            self.default_value = default


class Boolean(BaseObject):
    """Class for booleans."""

    default_value = False

    def get_schema(self):
        """Returns the object schema.

        Returns:
            dict. The object schema.
        """
        return {"type": "boolean", "description": self.description}


class NonnegativeInt(BaseObject):
    """Nonnegative integer class."""

    default_value = 0

    def get_schema(self):
        """Returns the object schema.

        Returns:
            dict. The object schema.
        """
        return {"type": "integer", "description": self.description, "minimum": 0}


class UnicodeString(BaseObject):
    """Unicode string class."""

    default_value = ""

    def get_schema(self):
        """Returns the object schema.

        Returns:
            dict. The object schema.
        """
        return {"type": "string", "description": self.description}


class Number(BaseObject):
    """numeric class."""

    default_value = None

    def get_schema(self):
        """Returns the object schema.

        Returns:
            dict. The object schema.
        """
        return {"type": ["null", "number"], "description": self.description}


class Array(BaseObject):
    """Array of items"""

    default_value = []

    def __init__(self, content_type="string", unique=True, min_items=1, **kwargs):
        super().__init__(**kwargs)
        self.content_type = content_type
        self.unique = unique
        self.min_items = min_items

    def get_schema(self):
        """Returns the object schema.

        Returns:
            dict. The object schema.
        """
        return {
            "type": "array",
            "items": {"type": self.content_type},
            "description": self.description,
            "uniqueItems": self.unique,
            "minItems": self.min_items,
        }
