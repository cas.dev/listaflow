"""
Tests for Workflow management commands
"""
from django.core.management import call_command

import pytest

from workflow.models import Run, RunTrendTaskReport
from workflow.tests.factories import ChecklistTaskDefinitionFactory
from workflow.tests.utils import create_team_and_checklist_definition

pytestmark = [
    pytest.mark.django_db(transaction=True),
    pytest.mark.usefixtures("celery_worker", "patch_flush_many_cache"),
]


def test_command_create_and_compute_trends_report():
    """Test command creating (if missing) and computing trends report for recurrence"""
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition()
    # One of these is already created by the previous call. Let's add one more.
    ChecklistTaskDefinitionFactory.create(checklist_definition=checklist_definition)
    Run.create_run_for_team(team, checklist_definition)
    assert RunTrendTaskReport.objects.count() == 2

    RunTrendTaskReport.objects.all().delete()
    assert RunTrendTaskReport.objects.count() == 0

    options = {"checklist_definition_id": checklist_definition.id}
    call_command("create_compute_trends_report", **options)
    assert RunTrendTaskReport.objects.count() == 2

    with pytest.raises(SystemExit):
        dummy_options = {"checklist_definition_id": "dummyid0"}
        call_command("create_compute_trends_report", **dummy_options)
