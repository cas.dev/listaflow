"""
General views that are not API endpoints.
"""
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render
from django.utils.translation import gettext_lazy as _

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response


def index(request: HttpRequest) -> HttpResponse:
    """
    Render index page.
    """
    return render(request, "build/index.html", {})


@api_view(("GET", "POST", "PATCH", "PUT", "HEAD", "DELETE", "OPTIONS"))
def bad_endpoint(request, *_args, **_kwargs):
    """
    return 404 error for bad api endpoint
    """
    return Response(
        status=status.HTTP_404_NOT_FOUND,
        data={
            "detail": _("{endpoint} is not a valid API Endpoint.").format(
                endpoint=request.path,
            )
        },
    )
