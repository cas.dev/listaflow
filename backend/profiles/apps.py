"""
Django app configuration for profiles.
"""
from django.apps import AppConfig


class ProfilesConfig(AppConfig):
    """
    Profiles configuration definition.
    """

    default_auto_field = "django.db.models.BigAutoField"
    name = "profiles"

    def ready(self):
        try:
            # pylint: disable=import-outside-toplevel disable=unused-import
            import profiles.signals
        except ImportError:
            pass
