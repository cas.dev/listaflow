"""
management commands for profile app
"""
import secrets

from oauth2_provider.management.commands.createapplication import Command as BaseCommand


class Command(BaseCommand):
    """
    Extends oauth's createapplication command to add generated secrets to env file.
    """

    help = (
        "Shortcut to create a new application in a programmatic way and "
        "update .env.local file with client_id and secret in frontend"
    )

    def add_arguments(self, parser):
        """
        adds option to provide env-file path
        """
        super().add_arguments(parser)
        parser.add_argument(
            "--env-file",
            type=str,
            help="Path to env file to add client_id and client_secret",
        )

    def handle(self, *args, **options):
        """
        generates client_id and client_secret if not provided and copies it to env file.
        """
        client_id = options.get("client_id")
        if not client_id:
            client_id = secrets.token_urlsafe(64)
            options["client_id"] = client_id
        client_secret = options.get("client_secret")
        if not client_secret:
            client_secret = secrets.token_urlsafe(128)
            options["client_secret"] = client_secret

        env_file = options.get("env_file")
        if not env_file:
            env_file = "../frontend/.env.local"
        env_vals = [
            f"REACT_APP_CLIENT_ID={client_id}\n",
            f"REACT_APP_CLIENT_SECRET={client_secret}\n",
        ]
        with open(env_file, "a+", encoding="utf-8") as file:
            file.writelines(env_vals)

        return super().handle(*args, **options)
