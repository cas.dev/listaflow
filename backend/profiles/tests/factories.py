"""
Factories useful for testing profiles module
"""
from django.contrib.auth import get_user_model
from factory import Faker, Sequence, PostGenerationMethodCall
from factory.django import DjangoModelFactory
from oauth2_provider.models import Application

from profiles.models import Team


class ApplicationFactory(DjangoModelFactory):
    """
    User factory.
    """

    client_type = "confidential"
    authorization_grant_type = "password"

    class Meta:
        model = Application


class TeamFactory(DjangoModelFactory):
    """
    Team factory.
    """

    class Meta:
        model = Team

    name = Faker("name")
    description = Faker("paragraph")


class UserFactory(DjangoModelFactory):
    """
    User factory.
    """

    username = Sequence(lambda x: f"user{x}")
    email = Sequence(lambda x: f"user{x}@example.com")
    password = PostGenerationMethodCall("set_password", "Test")

    class Meta:
        model = get_user_model()
