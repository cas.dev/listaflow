import { useSingle } from "@opencraft/providence/react-plugin";
import { fireEvent, render, screen } from "../../../utils/test-utils";
import { LinearScaleRate } from "../LinearScaleRate";
import { InterfaceComponentProps } from "../../../types/Interaction";
import { SingleController } from "@opencraft/providence/base/singles/types/SingleController";

let testController: SingleController<InterfaceComponentProps>;

const TestComponent = () => {
  testController = useSingle<InterfaceComponentProps>("testController", {
    endpoint: "#",
    x: {
      customizationArgs: {
        min_value: 1,
        max_value: 5,
        min_value_description: "Bad",
        max_value_description: "Good",
      },
      response: null,
    },
  });

  return (
    <LinearScaleRate
      customizationArgs={testController.x!.customizationArgs}
      response={testController.p.response}
    />
  );
};

describe("LinearScaleRate component", () => {
  beforeEach(() => {
    render(<TestComponent />);
  });

  it("should render list of rating items correctly", async () => {
    const ratingItems = screen.getAllByText(/\d/i);
    expect(ratingItems.length).toEqual(5);
    expect(ratingItems.map((i) => i.textContent)).toEqual([
      "1",
      "2",
      "3",
      "4",
      "5",
    ]);
  });

  it("should render min value description with correct style", async () => {
    const minValueDescription = screen.getByText("Bad");
    expect(minValueDescription).toBeInTheDocument();
    expect(minValueDescription.className).toContain("rating-description-label");
  });

  it("should render max value description with correct style", async () => {
    const maxValueDescription = screen.getByText("Good");
    expect(maxValueDescription).toBeInTheDocument();
    expect(maxValueDescription.className).toContain("rating-description-label");
  });

  it("should update response correctly on clicking a rating item", async () => {
    const ratingItemsWithValue3 = screen.getByText("3");
    fireEvent.click(ratingItemsWithValue3);

    expect(testController.p.response.model).toStrictEqual({ rating: 3 });
  });
});
