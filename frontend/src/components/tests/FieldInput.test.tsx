import { useForm } from "@opencraft/providence/react-plugin";
import { fireEvent, render, screen, waitFor } from "../../utils/test-utils";
import { FieldInput } from "../FieldInput";

interface TestFormState {
  test: string;
}

const TestComponent = () => {
  const fakeController = useForm<TestFormState>("fakeController", {
    endpoint: "#",
    fields: {
      test: {
        value: "",
        validators: [
          { name: "required" },
          { name: "length", args: { min: 8 } },
        ],
      },
    },
  });

  return (
    <FieldInput
      type="text"
      fielder={fakeController.f.test}
      label="test_label"
      name="test_name"
      id="test_id"
    />
  );
};

test("test field input component", async () => {
  render(<TestComponent />);

  const field = screen.getByText(/test_label/i);
  expect(field).toBeInTheDocument();

  fireEvent.blur(screen.getByLabelText("test_label"));
  await waitFor(() => screen.getByText(/required/i));

  const error = screen.getByText(/required/i);
  expect(error).toBeInTheDocument();

  const error2 = screen.getByText(/must contain at least/i);
  expect(error2).toBeInTheDocument();
});
