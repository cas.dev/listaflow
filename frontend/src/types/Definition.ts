export declare interface TaskDefinition {
  id: string;
  label: string;
  body: string;
}

export declare interface ShortDefinition {
  id: string;
  name: string;
}

export declare interface Definition extends ShortDefinition {
  body: string;
  task_definitions: Array<TaskDefinition>;
}

export declare interface Tag {
  id: number;
  name: string;
}
