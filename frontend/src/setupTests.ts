// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import "@testing-library/jest-dom";
import { createElement } from "react";
import crypto from "crypto";

global.ResizeObserver = require("resize-observer-polyfill");
window.URL.createObjectURL = jest.fn((str) => `Blob:${str}`);
window.URL.revokeObjectURL = jest.fn();

const MockResponsiveContainer = (props: any) => {
  return createElement("div", props);
};
jest.mock("recharts", () => ({
  ...jest.requireActual("recharts"),
  ResponsiveContainer: MockResponsiveContainer,
}));

export function testName() {
  return expect.getState().currentTestName;
}

export class LocalStorageMock {
  store: { [key: string]: { [key: string]: string } };
  constructor() {
    this.store = {};
  }

  clear() {
    delete this.store[testName()];
  }

  getItem(key: string) {
    const specStore = this.store[testName()] || {};
    return specStore[key] || null;
  }

  setItem(key: string, value: any) {
    const name = testName();
    if (!this.store[name]) {
      this.store[name] = {};
    }
    this.store[name][key] = value.toString();
  }

  removeItem(key: string) {
    delete this.store[testName()][key];
  }
}

Object.defineProperty(window, "localStorage", {
  value: new LocalStorageMock(),
});

Object.defineProperty(global, "crypto", {
  value: {
    getRandomValues: (arr: Uint8Array) => crypto.randomBytes(arr.length),
  },
});
